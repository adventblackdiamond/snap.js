module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        copy: {
            build: {
                files: [
                    {
                        expand: true,
                        cwd: './src',
                        src: 'snap.js',
                        dest: 'dist'
                    },
                    {
                        expand: true,
                        cwd: './src',
                        src: 'snap.css',
                        dest: 'dist'
                    }
                ]
            }
        },

        uglify: {
            build: {
                options: {
                    mangle: true,
                    sourceMap: true,
                    sourceMapName: 'dist/snap.min.map',
                    sourceMapIncludeSources: true,
                    compress: true
                },
                files: {
                    'dist/snap.min.js': [
                        './dist/snap.js'
                    ]
                }
            }
        },

        cssmin: {
            build: {
                options: {
                    sourceMap: true
                },
                files: [
                    {
                        expand: true,
                        cwd: 'dist/',
                        src: ['*.css', '!*.min.css'],
                        dest: 'dist/',
                        ext: '.min.css'
                    }
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('build', function() {
        grunt.task.run('copy:build');
        grunt.task.run('uglify:build');
        grunt.task.run('cssmin:build');
    });
};